﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            string userName = tbUserName.Text;
            string lastName = tbLastName.Text;
            string phoneNumber = tbPhoneNumber.Text;

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(lastName)
                || string.IsNullOrWhiteSpace(phoneNumber) || phoneNumber.Length != 9 || phoneNumber.Any(char.IsLetter))
            {
                tbUserName.Clear();
                tbLastName.Clear();
                tbPhoneNumber.Clear();
                return;
            }

            string listAdd = $"User name: {userName}, Last Name: {lastName}, Phone number: {phoneNumber}";

            lbAccounts.Items.Add(listAdd);
            tbUserName.Clear();
            tbLastName.Clear();
            tbPhoneNumber.Clear();
        }

        private void lbAccounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = lbAccounts.SelectedIndex;

            if (index == -1)
                return;

            var item1 = lbAccounts.Items[index];
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            int index = lbAccounts.SelectedIndex;

            if (index == -1)
                return;

            lbAccounts.Items.RemoveAt(index);
        }
    }
}
